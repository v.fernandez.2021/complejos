
class Complejos:
    def __init__(self,r,i):
        self.real=r
        self.imaginario=i
        
    def suma(self,other):
        suma_real=self.real+ other.real
        suma_imag=self.imaginario+other.imaginario
        return Complejos(suma_real,suma_imag)

    def resta(self,other):
        resta_real=self.real-other.real
        resta_imag=self.imaginario-other.imaginario
        return Complejos(resta_real,resta_imag)

    def multiplicacion(self,other):
        mutiplicacion_r1=self.real*other.real
        multiplicacion_i1=self.real*other.imaginario
        multiplicacion_r2=-(self.imaginario*other.imaginario)
        multiplicacion_i2=(self.imaginario*other.real)
        return Complejos(mutiplicacion_r1+multiplicacion_r2,multiplicacion_i1+multiplicacion_i2)

    def division(self,other):
        conjugado=Complejos(other.real,-other.imaginario)
        numerador=self.multiplicacion(conjugado)
        denominador=other.multiplicacion(conjugado)
        parte_real=numerador.real/denominador.real
        parte_imag=numerador.imaginario/denominador.real
        return  Complejos(parte_real,parte_imag)

    def __str__(self):
        if self.imaginario>=0:
            mensaje=" {real} + {imag} i".format(real=round(self.real,3),imag=round(self.imaginario,3))
        if self.imaginario<0:
            mensaje=" {real} - {imag} i".format(real=round(self.real,3),imag=round(abs(self.imaginario),3))
        return mensaje


