import unittest
from complejos1 import Complejos

class TestEmpleado(unittest.TestCase):

    def test_1(self):
        n1=Complejos(2,-3)
        self.assertEqual(n1.__str__()," 2 - 3 i")

    def test_2(self):
        n1=Complejos(2,-3)
        n2=Complejos(-5,3)
        self.assertEqual(Complejos.suma(n1,n2).__str__()," -3 + 0 i")

    def test_3(self):
        n1=Complejos(2,-3)
        n2=Complejos(-5,3)
        self.assertEqual(Complejos.resta(n1,n2).__str__()," 7 - 6 i")

    def test_4(self):
        n1=Complejos(2,-3)
        n2=Complejos(-5,3)
        self.assertEqual(Complejos.multiplicacion(n1,n2).__str__()," -1 + 21 i")

    def test_5(self):
        n1=Complejos(2,-3)
        n2=Complejos(-5,3)
        self.assertEqual(Complejos.division(n1,n2).__str__()," -0.559 + 0.265 i")

if __name__ == "__main__":
    unittest.main()
        

